<?php
    namespace app\utilities;

    use GuzzleHttp\Client;
    use yii\helpers\Json;

    class FlightApi
    {
        private $client;
        private $tempPlaces;
        private $tempQuotes;

        protected $apiKey = "your_key";
        protected $market = "PL";
        protected $currency = "PLN";
        protected $locale = "pl-PL";
        protected $baseUrl = "http://partners.api.skyscanner.net/apiservices/";

        public function __construct()
        {
            $this->client = new Client();
        }

        public function findAirportsByName($name){
            $response = $this->client->request('GET',
                $this->baseUrl . 'autosuggest/v1.0/' .
                $this->market . '/' .
                $this->currency . '/' .
                $this->locale . '/', [
                'query' => [
                    'query' => $name,
                    'apiKey' => $this->apiKey
                ],
                'headers' => [
                    "contentType" => "application/json"
                ]
            ]);

            $body = $response->getBody();

            return Json::decode($body->getContents(), false);
        }

        public function cheapestCityToAnywhere($place, $maxPrice = 200)
        {
            $results = [];
            $response = $this->client->request('GET',
                $this->baseUrl . 'browseroutes/v1.0/' .
                $this->market . '/' .
                $this->currency . '/' .
                $this->locale . '/' .
                $place->PlaceId . '/anywhere/anytime/anytime', [
                    'query' => [
                        'apiKey' => $this->apiKey
                    ],
                    'headers' => [
                        "contentType" => "application/json"
                    ]
                ]);

            $body = $response->getBody();
            $data = Json::decode($body->getContents(), false);

            $this->tempPlaces = $data->Places;
            $this->tempQuotes= $data->Quotes;

            foreach($data->Routes AS $key => $route){
                if(property_exists($route, "Price") && $route->Price <= $maxPrice){
                    if(property_exists($route, "QuoteIds") && !empty($route->QuoteIds)){
                        foreach($route->QuoteIds AS $quoteId){
                            $quote = $this->getQuoteData($quoteId);

                            if(strtotime($quote->OutboundLeg->DepartureDate) >= strtotime('today')){
                                $results[] = [
                                    'price' => $route->Price,
                                    'destination' => $this->getDestinationData($quote->OutboundLeg->DestinationId),
                                    'origin' => $place,
                                    'date' => $quote->OutboundLeg->DepartureDate
                                ];
                            }
                        }
                    }
                }
            }

            usort($results, function($a, $b){
                return $a['price'] - $b['price'];
            });

            return $results;
        }

        private function getDestinationData($placeId)
        {
            foreach($this->tempPlaces AS $place){
                if($place->PlaceId == $placeId){
                    return $place;
                }
            }

            return false;
        }

        private function getQuoteData($quoteId)
        {
            foreach($this->tempQuotes AS $quote){
                if($quote->QuoteId == $quoteId){
                    return $quote;
                }
            }

            return false;
        }
    }