<?php
    /* @var $this yii\web\View */
    /** @var SearchForm $model */

    use app\models\SearchForm;
    use yii\bootstrap\ActiveForm;
?>
<div class="searchIndex">
    <div class="col-lg-6 col-md-6 col-lg-offset-3 col-md-offset-3 col-sm-12 col-xs-12">
        <div id="searchForm">
            <?php $form = ActiveForm::begin([
                'action' => ['search/index']
            ]); ?>
                <div class="form-group types">
                    <p class="typeName">Losowa podróż</p>
                    <p class="typeDescription">Wyszukaj dowolną, najtańszą podróż w najbliższym terminie.</p>
                </div>

                <?= $form->field($model, 'cityFrom') ?>
                <?= $form->field($model, 'priceMax') ?>

            <div class="form-group text-right">
                <button class="btn btn-success">Wyszukaj</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
