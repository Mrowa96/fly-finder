<?php
    /**
     * @var array $airports
     * @var array $results
     */
?>
<div class="searchResults">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3>Wyniki wyszukiwania</h3>
        <?php if(!empty($airports)): ?>
            <?php if(!empty($results)): ?>
                <?php foreach($airports AS $airport): ?>
                    <div class="airportFlights row">
                        <div class="col-lg-12 col-md-12">
                            <h5 class="airportName">Lotnisko <strong><?= $airport->PlaceName ." (" . $airport->CountryName . ")";?></strong></h5>
                        </div>
                        <?php if(!empty($results[$airport->PlaceId])): ?>
                            <?php foreach($results[$airport->PlaceId] AS $result): ?>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="flight">
                                        <div class="destinationName">
                                            <span>Cel podróży:</span> <?= $result['destination']->Name . " (" . $result['destination']->CountryName. ")";?>
                                        </div>
                                        <div class="date">
                                            <span>Data wylotu:</span> <?= $result['date']; ?>
                                        </div>
                                        <div class="price">
                                            <span>Cena:</span> <strong><?= $result['price']; ?> PLN</strong>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <div class="col-lg-12 col-md-12">
                                <p class="error">Nie znaleziono żadnych lotów z tego lotniska</p>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <p class="error">Nie znaleziono żadnych lotów</p>
            <?php endif; ?>
        <?php else: ?>
            <p class="error">Nie znaleziono żadnego lotniska w podanej lokalizacji</p>
        <?php endif; ?>
    </div>
</div>
