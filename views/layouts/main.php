<?php
    /* @var $this \yii\web\View */
    /* @var $content string */

    use yii\helpers\Html;
    use yii\bootstrap\Nav;
    use yii\bootstrap\NavBar;
    use yii\widgets\Breadcrumbs;
    use app\assets\AppAsset;

    AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <?= Html::csrfMetaTags() ?>
            <title><?= Html::encode($this->title) ?></title>

            <?php $this->head() ?>
        </head>
        <body>
            <?php $this->beginBody() ?>
            <div class="wrapper">
                <div class="container-fluid">
                    <header class="row" id="header">
                        <div class="col-lg-12 col-md-12">
                            <a href="/">Fly finder</a>
                        </div>
                    </header>
                </div>
                <div class="container">
                    <div class="row">
                        <?php if(isset($content)): ?>
                            <?= $content; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <footer class="row" id="footer">
                    <div class="col-lg-12 col-md-12">
                        <span>Tavoite Paweł Mrowiec &copy; <?= date('Y') ?></span>
                    </div>
                </footer>
            </div>

            <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>
