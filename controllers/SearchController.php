<?php
    namespace app\controllers;

    use Yii;
    use app\utilities\FlightApi;
    use app\models\SearchForm;
    use yii\base\Controller;

    class SearchController extends Controller
    {
        protected $baseUrl = "http://partners.api.skyscanner.net/apiservices/";

        public function actionIndex()
        {
            $model = new SearchForm();
            $results = [];

            if($model->load(Yii::$app->request->post()) && $model->validate()){
                $api = new FlightApi();

                $airports = $api->findAirportsByName($model->cityFrom);

                if(!empty($airports->Places)){
                    foreach($airports->Places AS $place){
                        $results[$place->PlaceId] = $api->cheapestCityToAnywhere($place, $model->priceMax);
                    }
                }

                return $this->render('results', [
                    'airports' => $airports->Places,
                    'results' => $results
                ]);
            }

            return $this->render('index', [
                'model' => $model
            ]);
        }
    }