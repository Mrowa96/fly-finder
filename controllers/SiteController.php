<?php
    namespace app\controllers;

    use app\models\SearchForm;
    use Yii;
    use yii\web\Controller;

    class SiteController extends Controller
    {
        public function actions()
        {
            return [
                'error' => [
                    'class' => 'yii\web\ErrorAction',
                ],
            ];
        }

        public function beforeAction($action)
        {
            $this->view->title = 'Fly finder';

            return parent::beforeAction($action);
        }

        public function actionIndex()
        {
            return $this->redirect(['search/index']);
        }

    }
