<?php
    namespace app\models;

    use yii\base\Model;

    class SearchForm extends Model
    {
        public $cityFrom;
        public $priceMax;

        public function rules()
        {
            return [
                [['cityFrom', 'priceMax'], 'required'],
                ['cityFrom', 'string'],
                ['priceMax', 'double']
            ];
        }
        public function attributeLabels()
        {
            return [
                'cityFrom' => 'Miasto wylotu',
                'priceMax' => 'Maksymalna cena',
            ];
        }
    }