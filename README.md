Fly Finder
============================

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      config/             contains application configurations
      controllers/        contains Web controller classes
      extras/             contains extra data like example vhost or db dump        
      models/             contains model classes
      runtime/            contains files generated during runtime
      vendor/             contains dependent 3rd-party packages
      utilities/          contains utility classes
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.5.0.


INSTALLATION
------------
If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following commands:

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar install

~~~


CONFIGURATION
-------------
#### Vhost

Create your vhost file, it can be based on example vhost, which you can find in extras directory.
Remember that application has own .htaccess file.