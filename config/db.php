<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=fly-finder',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
