<?php
    namespace app\assets;

    use yii\web\AssetBundle;

    class AppAsset extends AssetBundle
    {
        public $basePath = '@webroot';
        public $baseUrl = '@web';
        public $css = [
            'styles/_fontAwesome/font-awesome.min.css',
            'styles/_bootstrap/bootstrap.min.css',
            'styles/_select2/select2.min.css',
            'styles/app.min.css'
        ];
        public $js = [
            'scripts/_bootstrap/bootstrap.min.js',
            'scripts/_select2/select2.min.js',
            'scripts/app.min.js'
        ];
        public $depends = [
            'yii\web\YiiAsset',
        ];
    }
